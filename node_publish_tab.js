(function () {
  //On value change of the selector, switch to the appropriate UI elements.
  jQuery("body").ready(function(){
    var $menu = jQuery("select[name='publish_option']");

    var $ui = jQuery("#" + $menu.val());
    $menu.change(function() {
      $ui.addClass("hidden");
      $ui = jQuery("#" + $menu.val());
      $ui.removeClass("hidden");
    });

    $menu.change();
  });
})();
